package com.example.app_proyecto.Fragments;

import com.example.app_proyecto.Notifications.MyResponse;
import com.example.app_proyecto.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAAHzAgG0I:APA91bH6p3Ek-fhs064ujLgY5uhyH7S1nhQNaoOX0LFNNq9GMkvGeWpv8WNojPbCEDzjC-N-rVUDJz2yOO5bDsnQpTtXKrXwxdiMz0IlxP6zilsiYRjPKmA0CrJdSxIYYNd_F2zfvob4"
    }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}
