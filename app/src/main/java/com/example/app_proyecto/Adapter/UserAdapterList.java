package com.example.app_proyecto.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.app_proyecto.MessageActivity;
import com.example.app_proyecto.Model.User;
import com.example.app_proyecto.Model.User_List;
import com.example.app_proyecto.R;

import java.util.List;

public class UserAdapterList extends RecyclerView.Adapter<UserAdapterList.ViewHolder> {

    private Context mContext;
    private List<User_List> mUsers;
    private boolean isChat;

    public UserAdapterList(Context mContext, List<User_List> mUsers, boolean isChat){
        this.mUsers = mUsers;
        this.mContext = mContext;
        this.isChat = isChat;
    }

    @NonNull
    @Override
    public UserAdapterList.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.user_item,parent,false);
        return new UserAdapterList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserAdapterList.ViewHolder holder, int position) {

        final User_List user = mUsers.get(position);
        holder.username.setText(user.getUsername());
        if (user.getImageURL().equals("default")){
            holder.profile_image.setImageResource(R.mipmap.ic_launcher);
        }else{
            Glide.with(mContext).load(user.getImageURL()).into(holder.profile_image);
        }

    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView username;
        public ImageView profile_image;


        public ViewHolder(View itemView){
            super(itemView);

            username = itemView.findViewById(R.id.username);
            profile_image = itemView.findViewById(R.id.profile_image);

        }
    }
}
