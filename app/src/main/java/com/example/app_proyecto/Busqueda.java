package com.example.app_proyecto;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.app_proyecto.Fragments.UsersFragment;

public class Busqueda extends AppCompatActivity {

    EditText t1,t2;

    Button ir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda);

        final UsersFragment usersFragment = new UsersFragment();
        FragmentManager manager = getSupportFragmentManager();
        final FragmentTransaction t = manager.beginTransaction();

        t1 = (EditText) findViewById(R.id.menor);
        t2 = (EditText) findViewById(R.id.mayor);

        ir = (Button) findViewById(R.id.button2);


        ir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b2 =  new Bundle();
                b2.putInt("MENOR", Integer.parseInt(t1.getText().toString()));
                b2.putInt("MAYOR", Integer.parseInt(t2.getText().toString()));
                usersFragment.setArguments(b2);
                t.add(R.id.frame1234,usersFragment);
                t.commit();
                startActivity(new Intent(Busqueda.this,ActividadEditarPerfil.class));
            }
        });
    }

}
