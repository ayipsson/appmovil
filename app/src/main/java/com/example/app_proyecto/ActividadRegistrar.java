package com.example.app_proyecto;


import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;

public class ActividadRegistrar extends AppCompatActivity {

    private Button buttonRegistrar ;

    private MaterialEditText username,edad,email,password;

    private CheckBox checkBoxhom,checkBoxmuj;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_registrar);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Registrar usuario");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseAuth= FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);

        checkBoxhom = findViewById(R.id.hombre);
        checkBoxmuj = findViewById(R.id.mujer);

        username=findViewById(R.id.editTextUser);
        edad=findViewById(R.id.date);
        email=findViewById(R.id.editTextCorreo);
        password=findViewById(R.id.editTextClave);
        buttonRegistrar=(Button)findViewById(R.id.buttonRegistrarDatos);

        buttonRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_username=username.getText().toString();
                Integer date= Integer.valueOf(edad.getText().toString());
                String txt_email=email.getText().toString();
                String txt_password=password.getText().toString();
                String hombre=checkBoxhom.getText().toString();
                String mujer=checkBoxmuj.getText().toString();

                if (TextUtils.isEmpty(txt_username)||TextUtils.isEmpty(txt_email)||TextUtils.isEmpty(txt_password)){
                    Toast.makeText(ActividadRegistrar.this,"Todos los campos son requeridos",Toast.LENGTH_SHORT).show();
                }else if (txt_password.length()<6){
                    Toast.makeText(ActividadRegistrar.this,"Contraseña no cuenta con 6 cáracteres mínimo",Toast.LENGTH_SHORT).show();
                }else {
                    registrarUsuario(txt_username,date,txt_email,txt_password,hombre,mujer);
                }
            }
        });
    }


    private void registrarUsuario(final String username, final Integer date, String email, String password, final String hombre, final String mujer){


        progressDialog.setMessage("Realizando registro");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        //checking if success
                        if(task.isSuccessful()){
                            Toast.makeText(ActividadRegistrar.this,"Se ha registrado el usuario con el nombre: "+ username+ "Verfique cuenta con correo electronico",Toast.LENGTH_LONG).show();

                            FirebaseUser firebaseUser=firebaseAuth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid=firebaseUser.getUid();

                            databaseReference= FirebaseDatabase.getInstance().getReference("Users").child(userid);
                            //linea agregada
                            firebaseUser.sendEmailVerification();

                            HashMap<String,String> hashMap=new HashMap<>();
                            hashMap.put("id",userid);
                            hashMap.put("username",username);
                            hashMap.put("edad", String.valueOf(date));
                            hashMap.put("genero_masculino",hombre);
                            hashMap.put("genero_femenino",mujer);
                            hashMap.put("imageURL","dafault");
                            hashMap.put("status","offline");
                            hashMap.put("search",username.toLowerCase());

                            databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Intent intent = new Intent(ActividadRegistrar.this, ActivityLogin.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        }else{
                            if(task.getException() instanceof FirebaseAuthUserCollisionException){//si se presenta una colision
                                Toast.makeText(ActividadRegistrar.this,"Ese usuario ya existe",Toast.LENGTH_LONG).show();
                            }else{
                                Toast.makeText(ActividadRegistrar.this,"No se pudo registrar el usuario ",Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

    }
}
